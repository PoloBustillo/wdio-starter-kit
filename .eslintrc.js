module.exports = {
    "plugins": ["webdriverio"],
    "env": {
    "webdriverio/wdio": true,
        "mocha": true
    },
    "parserOptions": {
        "ecmaVersion": 6
    },
    globals: {
      browser: false,
      expect: false,
      $: false,
      $$: false,
    }
};